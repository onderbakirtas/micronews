﻿using MicroNews.Models;
using MicroNews.Services;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace MicroNews.Controllers
{
    public class HomeController : Controller
    {
        private readonly LocalizationService localizationService;

        public HomeController(LocalizationService _localizationService)
        {
            localizationService = _localizationService;
        }

        public IActionResult Index()
        {
            ViewData["title"] = localizationService.Localize("brother");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
