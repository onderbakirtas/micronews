﻿using MicroNews.Services;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;

namespace MicroNews.Views
{
    public abstract class MyRazorPage<TModel> : RazorPage<TModel>
    {
        public string L(string key, string language = null)
        {
            var localizationService = Context.RequestServices.GetRequiredService<LocalizationService>();
            return localizationService.Localize(key, language);
        }
    }
}
