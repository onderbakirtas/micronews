﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace MicroNews.Services
{
    public class LocalizationService
    {
        private string DirectoryPath = null;
        private List<LocalFile> LocalFiles = null;

        private readonly CookieService cookieService;

        public LocalizationService(CookieService _cookieService)
        {
            cookieService = _cookieService;
        }

        public void Settings(string _directoryPath)
        {
            DirectoryPath = _directoryPath;
        }

        public void Refresh()
        {
            LocalFiles = new List<LocalFile>();

            if (DirectoryPath == null)
                return;

            var directoryFileNames = Directory.GetFiles(DirectoryPath);
            var correctFileNames = new List<string>();
            Regex regex = new Regex(@"^[a-z]{2}\.local\.json$");
            foreach (var fileName in directoryFileNames)
            {
                var replacedName = fileName.Replace(DirectoryPath + "\\","");
                if (regex.IsMatch(replacedName))
                {
                    var localFile = new LocalFile();
                    localFile.Language = replacedName.Replace(".local.json", "");
                    localFile.Content = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(fileName));
                    LocalFiles.Add(localFile);
                }
            }
        }

        public string JsCode()
        {
            return "";
        }

        public dynamic Localize(string key, string language = null)
        {
            if (LocalFiles == null)
                Refresh();

            if (language == null)
                language = cookieService.GetCulture();
                
            var localFile = LocalFiles.Where(e => e.Language == language).FirstOrDefault();
            if(localFile == null )
                return "-[" + key + "]-";

            var value = localFile.Content[key];
            if (value == null)
                return "[" + key + "]";
            
            return value;
        }
    }

    public class LocalFile
    {
        public string Language { get; set; }
        public dynamic Content { get; set; }
    }
}
