﻿using Microsoft.AspNetCore.Http;
using System;

namespace MicroNews.Services
{
    public class CookieService
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private string defaultCulture = "tr";
        private int? defaultExpireTime = 5 * 365 * 24 * 60 * 60;

        public CookieService(IHttpContextAccessor _httpContextAccessor)
        {
            httpContextAccessor = _httpContextAccessor;
        }

        public void SetDefaultCulture(string _defaultCulture)
        {
            if (_defaultCulture != null && _defaultCulture.Length == 2)
                defaultCulture = _defaultCulture;
        }

        public void SetDefaultExpireTime(int _defaultExpireTime)
        {
            defaultExpireTime = _defaultExpireTime;
        }

        public string GetCookie(string key)
        {
            return httpContextAccessor.HttpContext.Request.Cookies[key];
        }

        public void SetCookie(string key, string value, int? addExpireTimeBySecond = null)
        {
            if (addExpireTimeBySecond == null)
                addExpireTimeBySecond = defaultExpireTime;

            CookieOptions cookieOption = new CookieOptions() { Expires = DateTime.Now.AddSeconds((int)addExpireTimeBySecond) };
            httpContextAccessor.HttpContext.Response.Cookies.Append(key, value, cookieOption);
        }

        public string GetCulture()
        {
            var culture = GetCookie("currentCulture");
            if (culture == null)
                culture = defaultCulture;
            return culture;
        }
    }
}
